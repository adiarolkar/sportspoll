Steps:
1.	Node and NPM would be required to run this application on your local system. You can install it from: https://www.npmjs.com/get-npm
2. 	Open terminal.
3.	Go to a directory where you would like to clone the project.
4.	Enter command: ‘git clone https://adiarolkar@bitbucket.org/adiarolkar/sportspoll.git’ in the terminal and hit enter.
5.	Once the project is cloned, enter in the project directory.
6.	Enter command ‘npm install’ and hit enter.
7.	After all the required libraries are installed, enter command ‘npm start’ and hit enter.
8.	The browser will automatically open the URL and run the project.
