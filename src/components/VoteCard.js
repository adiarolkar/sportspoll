import React, { Component } from "react";
import cupImg from '../images/cup.png';
import timerImg from '../images/timer.png';

class VoteCard extends Component {
  constructor(props) {
    super(props);
      this.state = {
      };
      this.nextCard = this.nextCard.bind(this);
      this.submitVote = this.submitVote.bind(this);
    }
    submitVote(vote) {
      localStorage.setItem(JSON.stringify(this.props.entry.id), JSON.stringify(vote));
      this.nextCard();
    }

    nextCard() {
      this.props.nextCard();
    }
    render() {
        return (
            <div className="card">
              {
                this.props.entry.state === "STARTED" &&
                <img className="timer" src={timerImg}/>
              }
              <div className="sportname center-text">
                {this.props.entry.sport.split('_').join(' ')}
              </div>
              <div className="groupname center-text">
                <span className="imgicon">
                  <img src={cupImg}/>
                </span>
                <span className="groupname">
                  {this.props.entry.group}
                </span>
              </div>
              <div className="teams center-text">
                <span className="teamdp">
                  <span className="profile">
                    {this.props.entry.homeName.match(/\b\w/g).join('').substring(0, 2)}
                  </span>
                </span>
                <span className="vs">
                VS
                </span>
                <span className="teamdp">
                  <span className="profile">
                    {this.props.entry.awayName.match(/\b\w/g).join('').substring(0, 2)}
                  </span>
                </span>
              </div>
              <div className="countryname center-text">
                in {this.props.entry.country}
              </div>
              {
                this.props.entry.state === "STARTED" &&
                <h4 className="center-text alert m-t-30">
                  Note: This match has started
                </h4>
              }
              <div className="cardButtonBox m-t-30">
                <div className="button" onClick={()=>this.submitVote(this.props.entry.homeName)}>
                  {this.props.entry.homeName}
                </div>
                <div className="button center-button" onClick={()=>this.submitVote('Draw')}>
                  Draw
                </div>
                <div className="button" onClick={()=>this.submitVote(this.props.entry.homeName)}>
                  {this.props.entry.awayName}
                </div>
              </div>
            </div>
        );
    }
}

export default VoteCard;
