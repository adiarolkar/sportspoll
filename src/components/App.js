import React, { Component } from "react";
import WelcomePage from './WelcomePage';
import NavBar from './NavBar';
import MainPage from './MainPage';

import '../styles/App.scss';

class App extends Component {
    render() {
        return (
            <WelcomePage>
              <NavBar/>
              <MainPage/>
            </WelcomePage>
        );
    }
}

export default App;
