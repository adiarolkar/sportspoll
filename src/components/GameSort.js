import React, { Component } from "react";
import VoteCard from './VoteCard';
import LastCard from './LastCard';
import _ from 'lodash';
import data from '../../data/test-assignment.json';

class GameSort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSet: [],
      currentCard: '',
      currentCardCount: -1,
      totalCards: '',
      selectedSport: '',
      lastCard: false
    };
  }
  setRandomSport() {
    var randomnumber = Math.floor(Math.random() * (data.length + 1));
    var selectedSport = data[randomnumber].sport;
    this.getData(selectedSport);
  }
  getData(selectedSport) {
    var dataSet = [];
    _.forEach(data, function (entry, i) {
      if(selectedSport === entry.sport && entry.state !== 'FINISHED') {
        dataSet.push(entry)
      }
    });
    this.setState({
      selectedSport,
      dataSet,
      totalCards: dataSet.length
    },()=>{
      this.setCurrentCard();
    });

  }
  setCurrentCard() {
    console.log("here");
      var currentCardCount = this.state.currentCardCount + 1;
      var currentCard = this.state.dataSet[currentCardCount];
      console.log('currentCard',currentCard);
      if(currentCard) {
        this.setState({
          currentCard,
          currentCardCount
        }, ()=> {
          console.log('this.state',this.state);
        });
      } else {
        this.setState({
          lastCard:!this.state.lastCard
        });
      }
  }
  componentWillMount() {
    this.setRandomSport();
  }
  render() {
     //  var cards = this.state.dataSet.map(function(entry, i){
     //    currentCard
     //   return (<VoteCard key={i} entry={entry}/>);
     // });

      return (
        <div className="cardbox">
          {this.state.lastCard?
            <LastCard
            selectedSport={this.state.selectedSport}/>
            :
            this.state.currentCardCount > -1 &&
              <VoteCard
                nextCard={()=>this.setCurrentCard()}
                entry={this.state.currentCard}/>
          }
        </div>
      );
  }
}

export default GameSort;
