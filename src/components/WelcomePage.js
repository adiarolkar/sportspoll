import React, { Component } from "react";

class WelcomePage extends Component {
    render() {
        return (
            <div className="sportspoll">
                {this.props.children}
            </div>
        );
    }
}

export default WelcomePage;
