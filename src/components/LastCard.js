import React, { Component } from "react";

class lastcard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.refresh = this.refresh.bind(this);
  }
  refresh() {
    location.reload();
  }
  render() {

      return (
          <div className="lastcard card center-text">
            <h2 className="m-t-20">
              You have completed all the polls for {this.props.selectedSport.split('_').join(' ')}.
            </h2>
            <h3 className="m-t-20">
              Would you like to vote for another sport?
            </h3>
            <div className="cardButtonBox m-t-40">
              <div className="button" onClick={()=>this.refresh()}>
                Refresh
              </div>
            </div>
          </div>
      );
  }
}

export default lastcard;
